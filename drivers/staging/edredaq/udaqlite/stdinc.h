#ifndef _STDINC_H_
#define _STDINC_H_

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/kref.h>
#include <asm/uaccess.h>
#include <linux/usb.h>
#include <linux/mutex.h>

#include <edre/linuxio.h>
#include <edre/edrapi.h>
#include <edre/edreusb.h>
#include <edre/edredaq.h>
#include <edre/boards.h>
#include <edre/query.h>
#include <edre/errors.h>

#endif /*_STDINC_H_*/
/**
 * End of File
 */
