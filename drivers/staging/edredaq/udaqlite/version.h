/*******************************************************************************
 *                 Eagle Technology - Cape Town, South Africa
 *             Copyright (C) 2004 - 2009 - All Rights Reserved!
 *                 www.eagledaq.com - eagle@eagle.co.za
 *******************************************************************************/
#ifndef UDAQLITEDRV_VERSION

#define DRIVER_VERSION "v1.1.8"
#define UDAQLITEDRV_VERSION 0x00010108 /*1.1.8*/

#endif /*UDAQLITEDRV_VERSION*/
/**
 * End of File
 */
